#!/bin/bash

##################### CONST PARAMETERS #############################

#demo DS parameters
DEMO_DS_NAME=demo
DEMO_DS_ORIGIN=http://www.streambox.fr
DEMO_DS_TYPE=HTTP
DEMO_DS_URI=playlists/x31jrg1/x31jrg1.m3u8

#log and status files
ONE_CLICK_CDN_DIR=~/cdn_install
STATUS_FILE=$ONE_CLICK_CDN_DIR/status.json
LOG_FILE=$ONE_CLICK_CDN_DIR/install.log

#traffic Ops cookie
TMP_TO_COOKIE=
####################################################################

######################### FUNCTIONS ################################

function _usage() 
{
  ###### U S A G E : Help and ERROR ######
cat <<EOF
	Usage: one_click_cdn.sh <[options]>
	Options:
			-h   --help				Show this message
			--cmd-type				Command type (system_install/cache_install)
			--cdn-name				name of the CDN (no spaces)
			--cdn-domain			Domain of the CDN
			--host-resolve			Current host IP or FQDN
			--use-mid-cache			if set then MID cache preperation will be take action
			--cache-count			Number of cache that will be installed
			--controller-host		Traffic Ops public IP or FQDN
			--cache-type			Type of cache to install (EDGE/MID)
			--ds-name				Delivery service name
			--ds-origin				Delivery service origin URL
			--ds-type				Delivery service type (HTTP/HTTP LIVE/DNS/DNS LIVE)
EOF
  
	exit 1
}

check_docker_running() 
{
	DOCKER_NAME=$1
	log_message "checking $DOCKER_NAME for initialized"
	
	COUNTER=90
	
	until [  $COUNTER -lt 1 ]; do
			CHECK_RUNNING="$(sudo docker exec $DOCKER_NAME /bin/cat /etc/environment)"
			log_message "testing if $DOCKER_NAME $CHECK_RUNNING =~ INITIALIZED.*"
			
			if [[ "$CHECK_RUNNING" =~ INITIALIZED.* ]]; then
					log_message "docker $DOCKER_NAME was initialized"
					return
			fi
			
			sleep 1
			#let COUNTER-=1
	done
	
	log_message "Error: docker $DOCKER_NAME not running !!!"
}

function valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

function get_ops_cookie()
{
	TMP_TO_COOKIE="$(curl -v -s -k -X POST --data '{ "u":"'"$OPS_USER"'", "p":"'"$OPS_PASS"'" }' $OPS_URL_FROM_HOST/api/1.2/user/login 2>&1 | grep 'Set-Cookie' | sed -e 's/.*mojolicious=\(.*\); expires.*/\1/')"
	log_message "Ops cookie: $TMP_TO_COOKIE"	
}

function install_dependencies()
{
	write_log_and_status "1" "Install dependencies"
	
	#install required applications
	sudo yum -y install bind-utils curl docker
	sudo systemctl start docker
	sudo systemctl enable docker
}

function system_install()
{
	#pull dockers
	write_log_and_status "2" "Start pull system dockers"
	DOCKERS=(
		postgres
		influxdb
		gcr.io/vds-public-registry/traffic-ops:2.1.0
		gcr.io/vds-public-registry/traffic-portal:2.1.0
		gcr.io/vds-public-registry/traffic-stats:2.1.0
		gcr.io/vds-public-registry/traffic-vault:2.1.0
		gcr.io/vds-public-registry/traffic-monitor:2.1.0
		gcr.io/vds-public-registry/traffic-router:2.1.0
		gcr.io/vds-public-registry/traffic-stats:2.1.0
	)
	echo ${DOCKERS[*]} | xargs -P${#DOCKERS[@]} -n1 sudo docker pull
	write_log_and_status "3" "Finish pull system dockers"

	#create docker network
	log_message "create Docker network"
	sudo docker network create --subnet 192.168.0.0/24 --gateway 192.168.0.1 $NET_NAME

	#run traffic ops
	write_log_and_status "4" "Going to run Traffic Ops and Postgres DB"
	sudo docker run -itd --net $NET_NAME --name traffic-ops --hostname traffic-ops --publish $NET_OPS_PORT:443 postgres
	log_message "finish run postgres"
	sudo docker run -itd --net container:traffic-ops --name traffic-ops_app --env DB_SERVER=127.0.0.1 --env DB_PORT=5432 --env DB_USER=traffic_ops --env DB_NAME=traffic_ops --env ADMIN_USER=$OPS_USER --env ADMIN_PASS=$OPS_PASS --env CERT_COUNTRY=IL --env CERT_STATE=Israel --env CERT_CITY=Ceasarea --env CERT_COMPANY=HarmonicInc --env DOMAIN=$NET_NAME --env TRAFFIC_VAULT_PASS=$VAULT_PASS --env CDN_NAME=$CDN_NAME --env CDN_DNS_SUB_DOMAIN=$CDN_DOMAIN --env SERVICE_NAME=$DS_NAME --env SERVICE_TYPE=$DS_TYPE --env SERVICE_ORIGIN=$DS_ORIGIN --env USE_MID_CACHE=$USE_MID_CACHE gcr.io/vds-public-registry/traffic-ops:2.1.0
	log_message "finish run traffic ops"
	check_docker_running traffic-ops_app
	write_log_and_status "5" "Traffic Ops and Postgres DB are running"

	#run traffic vault
	write_log_and_status "6" "Going to run Traffic Vault"
	sudo docker run -itd --name traffic-vault --hostname traffic-vault --net $NET_NAME --env ADMIN_PASS=$VAULT_PASS --env USER_PASS=$VAULT_PASS --env TRAFFIC_OPS_URI=$OPS_URL_SAME_NET --env TRAFFIC_OPS_USER=$OPS_USER --env TRAFFIC_OPS_PASS=$OPS_PASS --env DOMAIN=$NET_NAME --env CDN_NAME=$CDN_NAME --env CREATE_TO_DB_ENTRY=YES gcr.io/vds-public-registry/traffic-vault:2.1.0
	log_message "finish run traffic vault"
	check_docker_running traffic-vault
	write_log_and_status "7" "Traffic Vault is running"

	#run traffic monitor
	write_log_and_status "8" "Going to run Traffic Monitor"
	sudo docker run -itd --publish $NET_MONITOR_PORT:80 --name traffic-monitor --hostname traffic-monitor --net $NET_NAME --env TRAFFIC_OPS_URI=$OPS_URL_SAME_NET --env TRAFFIC_OPS_USER=$OPS_USER --env TRAFFIC_OPS_PASS=$OPS_PASS --env DOMAIN=$NET_NAME --env CDN=$CDN_NAME --env IP=$HOST_PUBLIC_IP --env GATEWAY=$HOST_PUBLIC_GW --env ADITIONAL_ALLOW_IP="0.0.0.0/0" --env CREATE_TO_SERVER=YES gcr.io/vds-public-registry/traffic-monitor:2.1.0
	log_message "finish run traffic monitor"
	check_docker_running traffic-monitor
	write_log_and_status "9" "Traffic Monitor is running"

	#wait for all servers to start (EDGE and MID)
	get_ops_cookie
	ACTIVE_SERVERS_COUNT=0
	write_log_and_status "10" "Wait for $CONFIG_CACHE_COUNT cache servers"
	until [  $ACTIVE_SERVERS_COUNT -eq $CONFIG_CACHE_COUNT ]; do
		log_message "Wait for all cache servers. Active=$ACTIVE_SERVERS_COUNT Expected=$CONFIG_CACHE_COUNT"

		sleep 1
		ACTIVE_SERVERS_COUNT="$(curl -s -k -X GET -H "Cookie: mojolicious=$TMP_TO_COOKIE" $OPS_URL_FROM_HOST/api/1.2/servers.json | python -c 'import json,sys;obj=json.load(sys.stdin);match=[x["id"] for x in obj["response"] if x["type"]=="EDGE" or x["type"]=="MID"]; print len(match)')"
	done
	write_log_and_status "11" "All cache servers are up and running"

	#run traffic router
	write_log_and_status "12" "Going to run Traffic Router"
	sudo docker run -itd --publish 80:80 --publish 3333:3333 --publish 53:53 --publish 53:53/udp --name traffic-router --hostname traffic-router --net $NET_NAME --env TRAFFIC_OPS_URI=$OPS_URL_SAME_NET --env TRAFFIC_OPS_USER=$OPS_USER --env TRAFFIC_OPS_PASS=$OPS_PASS --env TRAFFIC_MONITORS="traffic-monitor.$NET_NAME:80" --env DOMAIN=$NET_NAME --env CDN_NAME=$CDN_NAME --env SERVICE=$DS_NAME --env IP=$HOST_PUBLIC_IP --env GATEWAY=$HOST_PUBLIC_GW gcr.io/vds-public-registry/traffic-router:2.1.0
	log_message "finish run traffic router"
	check_docker_running traffic-router
	write_log_and_status "13" "Traffic Router is running"

	#run traffic portal
	write_log_and_status "14" "Going to run Traffic Portal"
	sudo docker run -itd --publish $NET_PORTAL_PORT:80 --name traffic-portal --hostname traffic-portal --net $NET_NAME --env OPS_SERVER=$OPS_URL_SAME_NET gcr.io/vds-public-registry/traffic-portal:2.1.0
	log_message "finish run traffic portal"
	check_docker_running traffic-portal
	write_log_and_status "15" "Traffic Portal is running"

	#run traffic statistics
	write_log_and_status "16" "Going to run Traffic stats and influxdb"
	sudo docker run -itd --publish $NET_GRAFANA_PORT:3000 --name traffic-stats --hostname traffic-stats --net $NET_NAME influxdb
	log_message "finish run influxdb"
	sudo docker run -itd --net container:traffic-stats --name traffic-stats_app --env TRAFFIC_OPS_URI=$OPS_URL_SAME_NET --env TRAFFIC_OPS_USER=$OPS_USER --env TRAFFIC_OPS_PASS=$OPS_PASS --env INFLUXDB_URI=http://127.0.0.1:8086 --env INFLUXDB_HOSTNAME=traffic-stats --env DOMAIN=$NET_NAME --env CDN_NAME=$CDN_NAME --env CREATE_TO_DB_ENTRY=YES gcr.io/vds-public-registry/traffic-stats:2.1.0
	log_message "finish run traffic stats"
	check_docker_running traffic-stats_app
	write_log_and_status "17" "Traffic stats and InfluxDB are running"
	
	#create data sources in the traffic stats to see graphs
	write_log_and_status "18" "Create statistics sources"
	curl -v -s -k -X POST -H 'authorization: Basic YWRtaW46YWRtaW4=' -H 'content-type: application/json' --data '{ "name":"cache_stats", "type":"influxdb", "url":"http://localhost:8086", "access":"proxy", "basicAuth":false, "database":"cache_stats", "user":"influxUser", "password":"" }' http://127.0.0.1:3000/api/datasources
	curl -v -s -k -X POST -H 'authorization: Basic YWRtaW46YWRtaW4=' -H 'content-type: application/json' --data '{ "name":"deliveryservice_stats", "type":"influxdb", "url":"http://localhost:8086", "access":"proxy", "basicAuth":false, "database":"deliveryservice_stats", "user":"influxUser", "password":"" }' http://127.0.0.1:3000/api/datasources	
	write_log_and_status "19" "Statistics sources created"
}

function cache_install()
{
	#pull docker
	write_log_and_status "2" "Start pull cache docker"
	sudo docker pull gcr.io/vds-public-registry/traffic-server:2.1.0
	write_log_and_status "3" "Finish pull cache docker"

	#set parameters according to server type
	if [ "$CONFIG_CACHE_TYPE" = "EDGE" ] ; then
		CACHE_GROUP=edge-east
		TYPE=EDGE
		PROFILE=EDGE_ATS_621_CENTOS_721
		DOCKER_NAME=edge
	else
		CACHE_GROUP=mid-east
		TYPE=MID
		PROFILE=MID_ATS_532_CENTOS_721
		DOCKER_NAME=mid
	fi

	#run server docker
	write_log_and_status "4" "Going to run Cache server"
	sudo docker run -itd --privileged --name $DOCKER_NAME --net host --env TS_ROOT='/' --env TRAFFIC_OPS_URI=$OPS_URL_FROM_OUT --env TRAFFIC_OPS_USER=$OPS_USER --env TRAFFIC_OPS_PASS=$OPS_PASS --env DOMAIN=$NET_NAME --env CACHE_GROUP=$CACHE_GROUP --env TYPE=$TYPE --env PROFILE=$PROFILE --env STATUS=REPORTED --env CDN=$CDN_NAME --env SERVICE=$DS_NAME --env IP=$HOST_PUBLIC_IP --env GATEWAY=$HOST_PUBLIC_GW gcr.io/vds-public-registry/traffic-server:2.1.0
	log_message "finish run traffic server type $DOCKER_NAME"
	check_docker_running $DOCKER_NAME
	write_log_and_status "5" "Cache server is running"
}

function write_status_file()
{
	NEW_STATUS=$1
	NEW_URL=$2
	
	cat >$STATUS_FILE <<EOF
{
	"status": ${NEW_STATUS},
	"url": "${NEW_URL}"
}
EOF
}

function log_message()
{
	LOG_MSG=$1
	NOW_STR=`date`
	
	echo -e "$NOW_STR - $LOG_MSG" >> $LOG_FILE
}

function write_log_and_status()
{
	NEW_STATUS=$1
	LOG_MSG=$2
	
	log_message "$LOG_MSG"
	write_status_file $NEW_STATUS ""
}

####################################################################

####################### MAIN CODE ###################################

#create files directory
mkdir -p $ONE_CLICK_CDN_DIR

log_message "############################################################################"
log_message "Start running"
log_message "############################################################################"

#write status files
write_status_file "0" ""

#default values
CMD_TYPE=unknown
USE_MID_CACHE=NO
CONFIG_CACHE_COUNT=0
CONFIG_CACHE_TYPE=EDGE
DS_NAME=$DEMO_DS_NAME
DS_TYPE=$DEMO_DS_TYPE
DS_ORIGIN=$DEMO_DS_ORIGIN

[ $# = 0 ] && log_message "Invalid command" && _usage # "  >>>>>>>> no options given "
while getopts ':h-' OPTION ; do
	case "$OPTION" in
		h  ) _usage                         ;;   
		-  ) [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
			eval OPTION="\$$optind"
			OPTARG=$(echo $OPTION | cut -d'=' -f2)
			OPTION=$(echo $OPTION | cut -d'=' -f1)
			case $OPTION in
				--cmd-type			)	CMD_TYPE="$OPTARG"				;;
				--cdn-name       	)	CDN_NAME="$OPTARG"				;;
				--cdn-domain       	)	CDN_DOMAIN="$OPTARG"			;;
				--host-resolve    	)	CONFIG_HOST_RESOLVE="$OPTARG"	;;
				--use-mid-cache    	)	USE_MID_CACHE=YES				;;
				--cache-count		)	CONFIG_CACHE_COUNT="$OPTARG"	;;
				--controller-host	)	CONFIG_CONTROLLER_HOST="$OPTARG";;
				--cache-type		)	CONFIG_CACHE_TYPE="$OPTARG"		;;
				--ds-name			)	DS_NAME="$OPTARG"				;;
				--ds-origin			)	DS_TYPE="$OPTARG"				;;
				--ds-type			)	DS_ORIGIN="$OPTARG"				;;
				--help      		)	_usage							;;
				* 					)	_usage							;; # Long: >>>>>>>> invalid options (long)
			esac
			OPTIND=1
			shift
			;;
		? )	_usage														;; # Short: >>>>>>>> invalid options (short)
	esac
done

#if public ip is DNS name, then resolve it
log_message "CONFIG_HOST_RESOLVE = $CONFIG_HOST_RESOLVE"
if [[ $(valid_ip $CONFIG_HOST_RESOLVE) -eq 0 ]]; then
	HOST_PUBLIC_IP=$CONFIG_HOST_RESOLVE
else
	HOST_PUBLIC_IP=`nslookup $CONFIG_HOST_RESOLVE | grep Address | tail -n 1 | awk '{print $2}'`
fi
log_message "HOST_PUBLIC_IP = $HOST_PUBLIC_IP"


#local parameters
HOST_PUBLIC_GW=`echo "$HOST_PUBLIC_IP" | awk -F"." '{print $1"."$2"."$3".1"}'`
#HOST_DOMAIN=${CONFIG_HOST_RESOLVE#*.*}
NET_NAME=one.click.cdn
NET_OPS_PORT=8443
NET_MONITOR_PORT=8090
NET_PORTAL_PORT=8080
NET_GRAFANA_PORT=3000
OPS_USER=admin
OPS_PASS=harmonic
OPS_URL_SAME_NET=https://traffic-ops.$NET_NAME
OPS_URL_FROM_HOST=https://127.0.0.1:$NET_OPS_PORT
OPS_URL_FROM_OUT=https://$CONFIG_CONTROLLER_HOST:$NET_OPS_PORT
VAULT_PASS=harmonic

#set cache type to upper case
CONFIG_CACHE_TYPE=`echo $CONFIG_CACHE_TYPE | tr /a-z/ /A-Z/`

log_message "HOST_PUBLIC_GW = $HOST_PUBLIC_GW"
#log_message "HOST_DOMAIN = $HOST_DOMAIN"
log_message "OPS_URL_FROM_OUT = $OPS_URL_FROM_OUT"

#run requested command
log_message "Run command: $CMD_TYPE"
case "$CMD_TYPE" in
	"system_install"	)	install_dependencies; system_install			;;
	"cache_install"		)	install_dependencies; cache_install				;;
	*					)	echo "Invalid command type $CMD_TYPE"; _usage 	;;
esac


write_status_file "100" "http://tr.demo.$CDN_DOMAIN/$DEMO_DS_URI"
log_message "FINISH !!!"
echo "FINISH !!!"

exit 0

####################################################################
